import React, { Component } from 'react'
import {
  View,
  Text
} from 'react-native'

export default class Chat extends Component {
  render() {
    return (
      <View>
        <Text>
          Hello {this.props.name}
        </Text>
      </View>
    )
  }
}

// Chat.defaultProps = {
//   name: 'Elnur'
// }
//
// Chat.propTypes = {
//   name: React.PropTypes.string
// }
