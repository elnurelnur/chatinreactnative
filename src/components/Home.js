import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from 'react-native'

import {
  Actions
} from 'react-native-router-flux'

let styles = StyleSheet.create({
  title: {
    marginTop: 20,
    marginLeft: 20,
    fontSize: 20
  },
  nameInput: {
    padding: 5,
    height: 40,
    borderWidth: 2,
    borderColor: 'black'
  },
  buttonText: {
    marginLeft: 20,
    fontSize: 20,
    padding: 10,
    backgroundColor: 'red',
    width: 150
  }
})

export default class Home extends Component {
  state = {
    name: ''
  }

  render() {
    return (
      <View>
        <Text>Enter your name</Text>
        <TextInput
          style={styles.nameInput}
          placeholder='type name... Joh Mi'
          onChangeText={(text) => {
            this.setState({ name: text })
          }}
          value={this.state.name}
        />
        <TouchableOpacity
          style={styles.buttonText}
          onPress={() => {
            Actions.chat({
              name: this.state.name
            })
          }}
        >
          <Text>
            next
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
}
